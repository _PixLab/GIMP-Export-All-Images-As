;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License 
; as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.
; This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
; See the GNU General Public License for more details https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Created by PixLab 2023/12/12
; Download > https://www.gimp-forum.net/Thread-Export-ALL-opened-images-in-GIMP-at-once-with-your-settings
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;    WEBP      ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (script-fu-pxl-save-all-images-as-webp inDir inAskAfter inFileName inFileNumber inPreset inLossLess inQuality inAlphaQuality inAnimation inLoop inMiniSize inKeyframe inExif inIPTC inXMP inThumbnail inDelay inForceDelay runChoice)
  (let* (
          (i (car (gimp-image-list)))
          (ii (car (gimp-image-list)))
          (image)
          (isInteractive)         
          (newFileName "")
          (pathchar (if (equal?
                 (substring gimp-dir 0 1) "/") "/" "\\"))
          (webPreset)  
          (isLossless)    
          (qualityImage)
          (qualityAlpha)
          (isAnimation)
          (isLoop)
          (isMinimize)
          (keyframeNum)
          (isExif)
          (isIPTC)
          (isXMP)
          (isThumb)
          (delayNum)
          (isForceDelay)
        )
    (set! isInteractive
      (cond
        (( equal? runChoice 0 ) RUN-NONINTERACTIVE )
        (( equal? runChoice 1 ) RUN-INTERACTIVE )
        (( equal? runChoice 2 ) RUN-WITH-LAST-VALS )
      )
    )
    (set! webPreset                      ; re-order as I did "Picture" first
        (cond
        ((equal? inPreset 0) 1 )
        ((equal? inPreset 1) 2 )
        ((equal? inPreset 2) 3 )
        ((equal? inPreset 3) 4 )
        ((equal? inPreset 4) 5 )
        ((equal? inPreset 5) 0 )
        )
    )
    (set! isLossless
       (cond
       ((equal? inLossLess FALSE) 0 )
       ((equal? inLossLess TRUE) 1 )
       )
    )
    (set! qualityImage inQuality )
    (set! qualityAlpha inAlphaQuality )
    (set! isAnimation
       (cond
       ((equal? inAnimation FALSE) 0 )
       ((equal? inAnimation TRUE) 1 )
       )
    )
    (set! isLoop
       (cond
       ((equal? inLoop FALSE) 0 )
       ((equal? inLoop TRUE) 1 )
       )
    )
    (set! isMinimize
       (cond
       ((equal? inMiniSize FALSE) 0 )
       ((equal? inMiniSize TRUE) 1 )
       )
    )
    (set! keyframeNum inKeyframe)    
    (set! isExif
       (cond
       ((equal? inExif FALSE) 0 )
       ((equal? inExif TRUE) 1 )
       )
    )
    (set! isIPTC
       (cond
       ((equal? inIPTC FALSE) 0 )
       ((equal? inIPTC TRUE) 1 )
       )
    )
    (set! isXMP
      (cond
       ((equal? inXMP FALSE) 0 )
       ((equal? inXMP TRUE) 1 )
      )
    )
    (set! isThumb
      (cond
       ((equal? inThumbnail FALSE) 0 )
       ((equal? inThumbnail TRUE) 1 )
      )
    )
    (set! delayNum inDelay)
    (set! isForceDelay
      (cond
       ((equal? inForceDelay FALSE) 0 )
       ((equal? inForceDelay TRUE) 1 )
      )
    )

    (while (> i 0)

      (set! image (vector-ref (cadr (gimp-image-list)) (- i 1)))
      (set! newFileName (string-append inDir
              pathchar inFileName
              (substring "0000000" (string-length
              (number->string (+ inFileNumber i))))
              (number->string (+ inFileNumber (- ii i))) ".webp"))
     
          (file-webp-save2 isInteractive
                      image
                      (car (gimp-layer-new-from-visible image image "export"))
                      newFileName
                      newFileName
                      webPreset        ; preset 0=default 1=pic 2=photo 3=drawing 4=icon 5=text
                      isLossless       ; Use lossless encoding (0/1)
                      qualityImage     ; Quality of the image (0 <= quality <= 100)
                      qualityAlpha     ; alpha-quality  0<>100
                      isAnimation      ; Use layers for animation (0/1)
                      isLoop           ; Loop animation infinitely (0/1)
                      isMinimize       ; Minimize animation size (0/1)
                      keyframeNum      ; Maximum distance between key-frames (>=0)
                      isExif           ; Toggle saving exif data (0/1)
                      isIPTC           ; Toggle saving iptc data (0/1) works only if save XMP data is also checked ?
                      isXMP            ; Toggle saving xmp data (0/1)
                      isThumb          ; Toggle saving thumbnail (0/1)
                      delayNum         ; Delay to use when timestamps are not available or forced
                      isForceDelay     ; Force delay on all frames (0/1)

          )

      (if (not (= inAskAfter TRUE)) (gimp-image-clean-all image))
      (set! i (- i 1))


 )))

(script-fu-register "script-fu-pxl-save-all-images-as-webp"
 "1 - As WebP..."
 "Export all opened images at once as WebP with your settings..."
 "PixLab"
 "GPL-v2+"
 "2023/12/12"
 "*"
 SF-DIRNAME    "Select a directory to export your images" "Desktop"
 SF-TOGGLE     "Ask me to save the XCF when closing GIMP or an image" TRUE
 SF-STRING     "Input an image base name" "IMAGE-"
 SF-ADJUSTMENT "Input a start number for image name auto-numbering" (list 1 0 999999 1 100 0 SF-SPINNER)
 SF-OPTION     "Select a WebP Preset" (list "Picture" "Photo" "Drawing" "Icon" "Text" "Default")
 SF-TOGGLE     "Use lossless encoding" FALSE
 SF-ADJUSTMENT "Quality of the image" (list 90 0 100 1 1 0 SF-SLIDER)
 SF-ADJUSTMENT "Quality of Transparency" (list 90 0 100 1 1 0 SF-SLIDER)
 SF-TOGGLE     "Use Layers for Animation" FALSE
 SF-TOGGLE     "Loop Animation Infinitely" TRUE
 SF-TOGGLE     "Minimize Animation Size" FALSE
 SF-ADJUSTMENT "Maximum distance between key-frames:\n    0 = No Keyframes:\n    1 = All Frames are Keyframes" (list 50 0 10000 1 1 0 SF-SPINNER)
 SF-TOGGLE     "Save Exif Data" TRUE
 SF-TOGGLE     "Save IPTC Data" FALSE
 SF-TOGGLE     "Save XMP Data" FALSE
 SF-TOGGLE     "Save Thumbnail" FALSE
 SF-ADJUSTMENT "Delay to use between frames when unspecified" (list 100 0 10000 1 1 0 SF-SPINNER)
 SF-TOGGLE     "Use delay entered above for all frames" FALSE
 SF-OPTION     "Select How to Export (Explanations in the manual)" (list "Automated: Above settings for all images" "Full control: You confirm each setting on each image" "Use settings from latest Export")
 )
 (script-fu-menu-register "script-fu-pxl-save-all-images-as-webp" "<Image>/File/E_xport/Export All Images As")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  JPG  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (script-fu-pxl-save-all-images-as-jpg inDir inAskAfter inFileName inFileNumber inQuality inSmooth inHuffman inProgressive inComment inSubsampling inBaseLine inMCU inDCT runChoice)
  (let* (
          (i (car (gimp-image-list)))
          (ii (car (gimp-image-list)))
          (image)
          (isInteractive)          
          (newFileName "")
          (pathchar (if (equal?
                 (substring gimp-dir 0 1) "/") "/" "\\"))
          (isHuffman)
          (qualityJPG)
          (smoothJPG)
          (isHuffman)
          (isProgressive)
          (commentString "")
          (subSamplingSet)
          (isBaseLine)
          (isMCU)
          (isDCT)
        )

    (set! isInteractive
      (cond
        (( equal? runChoice 0 ) RUN-NONINTERACTIVE )
        (( equal? runChoice 1 ) RUN-INTERACTIVE )
        (( equal? runChoice 2 ) RUN-WITH-LAST-VALS )
      )
    )
    (set! qualityJPG (/ inQuality 100))   ; I did put the setting on the UI of 0 <> 100 to be user friendly but the scale in file-jpeg-save is 0 <> 1 float 2 decimal
    (set! smoothJPG inSmooth )
    (set! isHuffman
       (cond
       ((equal? inHuffman FALSE) 0 )
       ((equal? inHuffman TRUE) 1 )
       )
    )
    (set! isProgressive
       (cond
       ((equal? inProgressive FALSE) 0 )
       ((equal? inProgressive TRUE) 1)
       )
    )
    (set! commentString inComment)
    (set! subSamplingSet                 ; re-order as I did put "best quality" first to be default if user don't change
      (cond
        (( equal? inSubsampling 0 ) 2 )
        (( equal? inSubsampling 1 ) 0 )
        (( equal? inSubsampling 2 ) 1 )
        (( equal? inSubsampling 3 ) 3 )
      )
    )
    (set! isBaseLine
       (cond
       ((equal? inBaseLine FALSE) 0 )
       ((equal? inBaseLine TRUE) 1 )
       )
    )
    (set! isMCU inMCU)    
    (set! isDCT                         ; re-order as I did put "FLOAT" first to be default if user don't change
      (cond
        (( equal? inDCT 0 ) 2 )
        (( equal? inDCT 1 ) 0 )
        (( equal? inDCT 2 ) 1 )
      )
    )       
    (while (> i 0)

      (set! image (vector-ref (cadr (gimp-image-list)) (- i 1)))
      (set! newFileName (string-append inDir
              pathchar inFileName
              (substring "0000000" (string-length
              (number->string (+ inFileNumber i))))
              (number->string (+ inFileNumber (- ii i))) ".jpg"))
     
      (file-jpeg-save isInteractive
                      image
                      (car (gimp-layer-new-from-visible image image "export"))
                      newFileName
                      newFileName
                      qualityJPG        ;  Float Quality of saved image (0 => quality <= 1)
                      smoothJPG         ;  Float Smoothing factor for saved image (0 => smoothing <= 1)
                      isHuffman         ;  Use optimized tables during Huffman coding (0/1)
                      isProgressive     ;  Create progressive JPEG images (0/1)
                      commentString     ;  String Image comment (if you want to put "Image made by me!")
                      subSamplingSet    ;  Sub-sampling type { 0, 1, 2, 3 } 0 == 4:2:0 (chroma quartered), 1 == 4:2:2 Horizontal (chroma halved), 2 == 4:4:4 (best quality), 3 == 4:2:2 Vertical (chroma halved)
                      isBaseLine        ;  Force creation of a baseline JPEG (non-baseline JPEGs can't be read by all decoders) (0/1)
                      isMCU             ;  Interval of restart markers (in MCU rows, 0 = no restart markers)
                      isDCT             ;  DCT method to use { INTEGER (0), FIXED (1), FLOAT (2) }
      )

      (if (not (= inAskAfter TRUE)) (gimp-image-clean-all image))
      (set! i (- i 1))


 )))

(script-fu-register "script-fu-pxl-save-all-images-as-jpg"
 "2 - As JPG..."
 "Export all opened images at once as JPG with your settings..."
 "PixLab"
 "GPL-v2+"
 "2023/12/12"
 "*"
 SF-DIRNAME    "Select a directory to export your images" "Desktop"
 SF-TOGGLE     "Ask me to save the XCF when closing GIMP or an image" TRUE
 SF-STRING     "Input an image base name" "IMAGE-"
 SF-ADJUSTMENT "Input a start number for image name auto-numbering" (list 1 0 999999 1 100 0 SF-SPINNER)
 SF-ADJUSTMENT "Image Quality ( > 95 Not recommended, RTM)" (list 88 0 100 1 1 0 SF-SLIDER)
 SF-ADJUSTMENT "Smoothing (can help reduce file size)" (list 0.00 0.00 1.00 0.01 0.01 2 SF-SLIDER)
 SF-TOGGLE     "Use optimized tables during Huffman coding" TRUE
 SF-TOGGLE     "Create progressive JPEG (NOT recommended for software compatibility)" FALSE
 SF-STRING     "Input a comment inside your image" "@ made with GIMP"
 SF-OPTION     "Sub-sampling type" (list "4:4:4 (best quality)" "4:2:0 (chroma quartered)" "4:2:2 Horizontal (chroma halved)" "4:2:2 Vertical (chroma halved)")
 SF-TOGGLE     "Force baseline JPEG (recommended for software compatibility)" TRUE
 SF-ADJUSTMENT "Restart markers MCU rows (0 = no restart, recommended)" (list 0 0 64 1 1 0 SF-SPINNER)
 SF-OPTION     "DCT method to use" (list "Float" "Integer" "Fixed")
 SF-OPTION     "Select How to Export (Explanations in the manual)" (list "Automated: Above settings for all images" "Full control: You confirm each setting on each image" "Use settings from latest Export")
 )
 (script-fu-menu-register "script-fu-pxl-save-all-images-as-jpg" "<Image>/File/E_xport/Export All Images As")
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;   PNG   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (script-fu-pxl-save-all-images-as-png inDir inAskAfter inFileName inFileNumber inAdam inDeflate inBackground inGamma inOffset inResolution inDate inComment inTransparentColor runChoice)
  (let* (
          (i (car (gimp-image-list)))
          (ii (car (gimp-image-list)))
          (image)
          (isInteractive)         
          (newFileName "")
          (pathchar (if (equal?
                 (substring gimp-dir 0 1) "/") "/" "\\"))
          (isAdam)  
          (isDeflate)    
          (isBackground)
          (isGamma)
          (isOffset)
          (isResolution)
          (isDate)
          (isComment)
          (isTransparentColor)
        )
    (set! isInteractive
      (cond
        (( equal? runChoice 0 ) RUN-NONINTERACTIVE )
        (( equal? runChoice 1 ) RUN-INTERACTIVE )
        (( equal? runChoice 2 ) RUN-WITH-LAST-VALS )
      )
    )
    (set! isAdam
       (cond
       ((equal? inAdam FALSE) 0 )
       ((equal? inAdam TRUE) 1 )
       )
    )
    (set! isDeflate inDeflate )
    (set! isBackground
       (cond
       ((equal? inBackground FALSE) 0 )
       ((equal? inBackground TRUE) 1 )
       )
    )
    (set! isGamma
       (cond
       ((equal? inGamma FALSE) 0 )
       ((equal? inGamma TRUE) 1 )
       )
    )
    (set! isOffset
       (cond
       ((equal? inOffset FALSE) 0 )
       ((equal? inOffset TRUE) 1 )
       )
    )   
    (set! isResolution
       (cond
       ((equal? inResolution FALSE) 0 )
       ((equal? inResolution TRUE) 1 )
       )
    )
    (set! isDate
       (cond
       ((equal? inDate FALSE) 0 )
       ((equal? inDate TRUE) 1 )
       )
    )
    (set! isComment
      (cond
       ((equal? inComment FALSE) 0 )
       ((equal? inComment TRUE) 1 )
      )
    )
    (set! isTransparentColor
      (cond
       ((equal? inTransparentColor FALSE) 0 )
       ((equal? inTransparentColor TRUE) 1 )
      )
    )

    (while (> i 0)

      (set! image (vector-ref (cadr (gimp-image-list)) (- i 1)))
      (set! newFileName (string-append inDir
              pathchar inFileName
              (substring "0000000" (string-length
              (number->string (+ inFileNumber i))))
              (number->string (+ inFileNumber (- ii i))) ".png"))
     
           (file-png-save2 isInteractive
                      image
                      (car (gimp-layer-new-from-visible image image "export"))
                      newFileName
                      newFileName
                      isAdam                ; Use Adam7 interlacing? (0/1)
                      isDeflate             ; Deflate Compression factor (0--9)
                      isBackground          ; Save Background Color? (0/1)
                      isGamma               ; Save gAMA chunk? (0/1)
                      isOffset              ; Save Layer oFFsets chunk? (0/1)
                      isResolution          ; Save Resolution chunk? (0/1)
                      isDate                ; Save creation date chunk? (0/1)
                      isComment             ; Write comment (taken from GIMP comment)
                      isTransparentColor    ; Preserve color of transparent pixels? (0/1)

          )

      (if (not (= inAskAfter TRUE)) (gimp-image-clean-all image))
      (set! i (- i 1))


 )))

(script-fu-register "script-fu-pxl-save-all-images-as-png"
 "3 - As PNG..."
 "Export all opened images at once as PNG with your settings..."
 "PixLab"
 "GPL-v2+"
 "2023/12/12"
 "*"
 SF-DIRNAME    "Select a directory to export your images" "Desktop"
 SF-TOGGLE     "Ask me to save the XCF when closing GIMP or an image" TRUE
 SF-STRING     "Input an image base name" "IMAGE-"
 SF-ADJUSTMENT "Input a start number for image name auto-numbering" (list 1 0 999999 1 100 0 SF-SPINNER)
 SF-TOGGLE     "Use Adam7 interlacing?" FALSE
 SF-ADJUSTMENT "Compression factor (0-9)" (list 9 0 9 1 1 0 SF-SLIDER)
 SF-TOGGLE     "Save Background Color" TRUE
 SF-TOGGLE     "Save gamma" FALSE
 SF-TOGGLE     "Save layer offset" FALSE
 SF-TOGGLE     "Save resolution" TRUE
 SF-TOGGLE     "Save creation time" TRUE
 SF-TOGGLE     "Save comment" TRUE
 SF-TOGGLE     "Preserve color of transparent pixels?" FALSE
 SF-OPTION     "Select How to Export (Explanations in the manual)" (list "Automated: Above settings for all images" "Full control: You confirm each setting on each image" "Use settings from latest Export")
 )
 (script-fu-menu-register "script-fu-pxl-save-all-images-as-png" "<Image>/File/E_xport/Export All Images As")
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  TIFF  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (script-fu-pxl-save-all-images-as-tif inDir inAskAfter inFileName inFileNumber inCompression runChoice)  ;inKeepColor inBigTiff
  (let* (
          (i (car (gimp-image-list)))
          (ii (car (gimp-image-list)))
          (image)
          (isInteractive)         
          (newFileName "")
          (pathchar (if (equal?
                 (substring gimp-dir 0 1) "/") "/" "\\"))
          (isCompression)  
;          (isKeepColor)    
;          (isBigTiff)
        )
    (set! isInteractive
      (cond
        (( equal? runChoice 0 ) RUN-NONINTERACTIVE )
        (( equal? runChoice 1 ) RUN-INTERACTIVE )
        (( equal? runChoice 2 ) RUN-WITH-LAST-VALS )
      )
    )
    (set! isCompression                    ; re-order as I did "DEFLATE" first
       (cond
        (( equal? inCompression 0 ) 3 )
        (( equal? inCompression 1 ) 1 )
        (( equal? inCompression 2 ) 2 )
        (( equal? inCompression 3 ) 4 )
        (( equal? inCompression 4 ) 5 )
        (( equal? inCompression 5 ) 6 )
        (( equal? inCompression 6 ) 0 )
       )
    )
;    (set! isKeepColor
;       (cond
;       ((equal? inKeepColor FALSE) 0 )
;       ((equal? inKeepColor TRUE) 1 )
;       )
;    )
;    (set! isBigTiff
;       (cond
;       ((equal? inBigTiff FALSE) 0 )
;       ((equal? inBigTiff TRUE) 1 )
;       )
;    )

    (while (> i 0)

      (set! image (vector-ref (cadr (gimp-image-list)) (- i 1)))
      (set! newFileName (string-append inDir
              pathchar inFileName
              (substring "0000000" (string-length
              (number->string (+ inFileNumber i))))
              (number->string (+ inFileNumber (- ii i))) ".tif"))
     
           (file-tiff-save isInteractive  ; file-bigtiff-save
                      image
                      (car (gimp-layer-new-from-visible image image "export"))
                      newFileName
                      newFileName
                      isCompression      ; Compression type: { NONE (0), LZW (1), PACKBITS (2), DEFLATE (3), JPEG (4), CCITT G3 Fax (5), CCITT G4 Fax (6) }
                      ;isKeepColor       ; Keep the color data masked by an alpha channel intact (do not store premultiplied components) (0/1)
                      ;isBigTiff         ; Export in BigTIFF variant file format up to 18 exabytes (18000 PB petabytes) (0/1)
                     
          )

      (if (not (= inAskAfter TRUE)) (gimp-image-clean-all image))
      (set! i (- i 1))


 )))

(script-fu-register "script-fu-pxl-save-all-images-as-tif"
 "4 - As TIFF..."
 "Export all opened images at once as TIFF with your settings..."
 "PixLab"
 "GPL-v2+"
 "2023/12/12"
 "*"
 SF-DIRNAME    "Select a directory to export your images" "Desktop"
 SF-TOGGLE     "Ask me to save the XCF when closing GIMP or an image" TRUE
 SF-STRING     "Input an image base name" "IMAGE-"
 SF-ADJUSTMENT "Input a start number for image name auto-numbering" (list 1 0 999999 1 100 0 SF-SPINNER)
 SF-OPTION     "Compression type" (list "DEFLATE" "LZW" "PACKBITS" "JPEG" "CCITT G3 Fax" "CCITT G4 Fax" "None...") 
; SF-TOGGLE     "Keep the color data masked by an alpha channel intact" TRUE
; SF-TOGGLE     "Export in BigTIFF variant up to 18000 PB (petabytes)" FALSE
 SF-OPTION     "Select How to Export (Explanations in the manual)" (list "Automated: Above settings for all images" "Full control: You confirm each setting on each image" "Use settings from latest Export")
 )
 (script-fu-menu-register "script-fu-pxl-save-all-images-as-tif" "<Image>/File/E_xport/Export All Images As")
 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  BMP  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (script-fu-pxl-save-all-images-as-bmp inDir inAskAfter inFileName inFileNumber inRLE inColorSpace inRGBformat runChoice) 
  (let* (
          (i (car (gimp-image-list)))
          (ii (car (gimp-image-list)))
          (image)
          (isInteractive)         
          (newFileName "")
          (pathchar (if (equal?
                 (substring gimp-dir 0 1) "/") "/" "\\"))
          (isRLE)  
          (isColorSpace)    
          (isRGBformat)
        )
    (set! isInteractive
      (cond
        (( equal? runChoice 0 ) RUN-NONINTERACTIVE )
        (( equal? runChoice 1 ) RUN-INTERACTIVE )
        (( equal? runChoice 2 ) RUN-WITH-LAST-VALS )
      )
    )
    (set! isRGBformat                     ; re-order as I did "32-bit RGBA 8888" first
       (cond
        (( equal? inRGBformat 0 ) 4 )
        (( equal? inRGBformat 1 ) 5 )
        (( equal? inRGBformat 2 ) 3 )
        (( equal? inRGBformat 3 ) 0 )
        (( equal? inRGBformat 4 ) 1 )
        (( equal? inRGBformat 5 ) 2 )
       )
    )
    (set! isColorSpace
       (cond
       ((equal? inColorSpace FALSE) 0 )
       ((equal? inColorSpace TRUE) 1 )
       )
    )
    (set! isRLE
       (cond
       ((equal? inRLE FALSE) 0 )
       ((equal? inRLE TRUE) 1 )
       )
    )

    (while (> i 0)

      (set! image (vector-ref (cadr (gimp-image-list)) (- i 1)))
      (set! newFileName (string-append inDir
              pathchar inFileName
              (substring "0000000" (string-length
              (number->string (+ inFileNumber i))))
              (number->string (+ inFileNumber (- ii i))) ".bmp"))
     
           (file-bmp-save2 isInteractive
                      image
                      (car (gimp-layer-new-from-visible image image "export"))
                      newFileName
                      newFileName
                      isRLE              ; Use run-length-encoding (RLE) compression (only valid for 4 and 8-bit indexed images)
                      isColorSpace       ; write-color-space Whether or not to write BITMAPV5HEADER color space data 0 = write, 1 = do not write
                      isRGBformat        ; Export format for RGB images (0=RGB_565, 1=RGBA_5551, 2=RGB_555, 3=RGB_888, 4=RGBA_8888, 5=RGBX_8888)
                     
          )

      (if (not (= inAskAfter TRUE)) (gimp-image-clean-all image))
      (set! i (- i 1))


 )))

(script-fu-register "script-fu-pxl-save-all-images-as-bmp"
 "5 - As BMP..."
 "Export all opened images at once as BMP with your settings..."
 "PixLab"
 "GPL-v2+"
 "2023/12/12"
 "*"
 SF-DIRNAME    "Select a directory to export your images" "Desktop"
 SF-TOGGLE     "Ask me to save the XCF when closing GIMP or an image" TRUE
 SF-STRING     "Input an image base name" "IMAGE-"
 SF-ADJUSTMENT "Input a start number for image name auto-numbering" (list 1 0 999999 1 100 0 SF-SPINNER)
 SF-TOGGLE     "RLE Compression (Only valid for 4 & 8-bit indexed images)" FALSE
 SF-TOGGLE     "Do Not Write BITMAPV5HEADER color space data" FALSE
 SF-OPTION     "Advanced options: Export format for RGB images" (list "32-bit RGBA 8888" "32-bit RGBX 8888" "24-bit RGB 888" "16-bit RGB 565" "16-bit RGBA 5551" "16-bit RGB 555")
 SF-OPTION     "Select How to Export (Explanations in the manual)" (list "Automated: Above settings for all images" "Full control: You confirm each setting on each image" "Use settings from latest Export")
 )
 (script-fu-menu-register "script-fu-pxl-save-all-images-as-bmp" "<Image>/File/E_xport/Export All Images As")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  PDF  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (script-fu-pxl-save-all-images-as-pdf inDir inAskAfter inFileName inFileNumber inVector inOmit inMask inPage inReverse runChoice) 
  (let* (
          (i (car (gimp-image-list)))
          (ii (car (gimp-image-list)))
          (image)
          (isInteractive)         
          (newFileName "")
          (pathchar (if (equal?
                 (substring gimp-dir 0 1) "/") "/" "\\"))
          (isVector)  
          (isOmit)    
          (isMask)
          (isPage)
          (isReverse)
        )
    (set! isInteractive
      (cond
        (( equal? runChoice 0 ) RUN-NONINTERACTIVE )
        (( equal? runChoice 1 ) RUN-INTERACTIVE )
        (( equal? runChoice 2 ) RUN-WITH-LAST-VALS )
      )
    )
    (set! isVector                     
       (cond
       ((equal? inVector FALSE) 0 )
       ((equal? inVector TRUE) 1 )
       )
    )
    (set! isOmit
       (cond
       ((equal? inOmit FALSE) 0 )
       ((equal? inOmit TRUE) 1 )
       )
    )
    (set! isMask
       (cond
       ((equal? inMask FALSE) 0 )
       ((equal? inMask TRUE) 1 )
       )
    )
    (set! isPage
       (cond
       ((equal? inPage FALSE) 0 )
       ((equal? inPage TRUE) 1 )
       )
    )
    (set! isReverse
       (cond
       ((equal? inReverse FALSE) 0 )
       ((equal? inReverse TRUE) 1 )
       )
    )

    (while (> i 0)

      (set! image (vector-ref (cadr (gimp-image-list)) (- i 1)))
      (set! newFileName (string-append inDir
              pathchar inFileName
              (substring "0000000" (string-length
              (number->string (+ inFileNumber i))))
              (number->string (+ inFileNumber (- ii i))) ".pdf"))
     
           (file-pdf-save2 isInteractive
                      image
                      (car (gimp-layer-new-from-visible image image "export"))
                      newFileName
                      newFileName
                      isVector                  ; Convert bitmaps to vector graphics where possible. TRUE or FALSE
                      isOmit                    ; Omit hidden layers and layers with zero opacity. TRUE or FALSE
                      isMask                    ; Apply layer masks before saving. TRUE or FALSE (Keeping them will not change the output)
                      isPage                    ; Layers as pages (bottom layers first). TRUE or FALSE
                      isReverse                 ; Reverse the pages order (top layers first). TRUE or FALSE
          )

      (if (not (= inAskAfter TRUE)) (gimp-image-clean-all image))
      (set! i (- i 1))


 )))

(script-fu-register "script-fu-pxl-save-all-images-as-pdf"
 "6 - As PDF..."
 "Export all opened images at once as PDF with your settings..."
 "PixLab"
 "GPL-v2+"
 "2023/12/12"
 "*"
 SF-DIRNAME    "Select a directory to export your images" "Desktop"
 SF-TOGGLE     "Ask me to save the XCF when closing GIMP or an image" TRUE
 SF-STRING     "Input a PDF base name" "PDF-"
 SF-ADJUSTMENT "Input a start number for PDF name auto-numbering" (list 1 0 999999 1 100 0 SF-SPINNER)
 SF-TOGGLE     "Convert bitmaps to vector graphics where possible." TRUE
 SF-TOGGLE     "Omit hidden layers and layers with zero opacity." TRUE
 SF-TOGGLE     "Apply layer masks before saving." TRUE
 SF-TOGGLE     "Layers as pages (bottom layers first)" TRUE
 SF-TOGGLE     "Reverse the pages order (top layers first)" FALSE
 SF-OPTION     "Select How to Export (Explanations in the manual)" (list "Automated: Above settings for all images" "Full control: You confirm each setting on each image" "Use settings from latest Export")
 )
 (script-fu-menu-register "script-fu-pxl-save-all-images-as-pdf" "<Image>/File/E_xport/Export All Images As")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;  ORA  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (script-fu-pxl-save-all-images-as-ora inDir inAskAfter inFileName inFileNumber runChoice) 
  (let* (
          (i (car (gimp-image-list)))
          (ii (car (gimp-image-list)))
          (image)
          (isInteractive)         
          (newFileName "")
          (pathchar (if (equal?
                 (substring gimp-dir 0 1) "/") "/" "\\"))

        )
    (set! isInteractive
      (cond
        (( equal? runChoice 0 ) RUN-NONINTERACTIVE )
        (( equal? runChoice 1 ) RUN-INTERACTIVE )
        (( equal? runChoice 2 ) RUN-WITH-LAST-VALS )
      )
    )

    (while (> i 0)

      (set! image (vector-ref (cadr (gimp-image-list)) (- i 1)))
      (set! newFileName (string-append inDir
              pathchar inFileName
              (substring "0000000" (string-length
              (number->string (+ inFileNumber i))))
              (number->string (+ inFileNumber (- ii i))) ".ora"))
     
           (file-openraster-save isInteractive
                      image
                      (car (gimp-layer-new-from-visible image image "export"))
                      newFileName
                      newFileName
                     
          )

      (if (not (= inAskAfter TRUE)) (gimp-image-clean-all image))
      (set! i (- i 1))


 )))

(script-fu-register "script-fu-pxl-save-all-images-as-ora"
 "7 - As ORA..."
 "Export all opened images at once as ORA (OpenRaster) with your settings..."
 "PixLab"
 "GPL-v2+"
 "2024/09/25"
 "*"
 SF-DIRNAME    "Select a directory to export your images" "Desktop"
 SF-TOGGLE     "Ask me to save the XCF when closing GIMP or an image" TRUE
 SF-STRING     "Input an image base name" "IMAGE-"
 SF-ADJUSTMENT "Input a start number for image name auto-numbering" (list 1 0 999999 1 100 0 SF-SPINNER)
 SF-OPTION     "Select How to Export (Explanations in the manual)" (list "Automated: Above settings for all images" "Full control: You confirm each setting on each image" "Use settings from latest Export")
 )
 (script-fu-menu-register "script-fu-pxl-save-all-images-as-ora" "<Image>/File/E_xport/Export All Images As")