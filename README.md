# GIMP-Export-All-Images-As
Export all opened images in GIMP as JPG, PNG, WebP, BMP, ORA, PDF, or TIFF at once

### Direct download > https://gitlab.com/_PixLab/GIMP-Export-All-Images-As/-/raw/main/pxl-export-all-images-as.7z?ref_type=heads
Once downloaded, just unzip-decompress the file. There is 3 files inside:
1) pxl-export-all-images-as.scm is **for GIMP 2.10 only**
2) gimp3-pxl-export-all-images-as.scm is **for GIMP 3 only**
3) pxl-export-all-images-as-GUIDE-MANUAL.pdf is the user manual, **that is worth reading**

# Purpose.
Batch export all opened images in GIMP at once without the need to export them one by one, nor the need to interact with the user interface each time an image is exported as everything can be automated.

### Use case example: 
You sliced your image from a guide’s grid of 10x10, and you have 100 new opened images (red rectangle below).
Do you want to export them as JPG or PNG one by one? That’s why this script is for, to batch export all these opened images at once.
Or simply, like everyday, you just did work on a dozen of images at a time and want them to be exported ASAP, this script will do the job in a matter of seconds.

### Recommendation: 
This script will override any existing image with the same name and extension, even though it will auto-number each exported image to not match an existing name, it’s better that you create a new folder for the exported images.

![screenshot_20231215-140503-1](https://github.com/PatLabGit/GIMP-Export-All-Images-As/assets/47497693/2c65e08d-8396-4d55-99a1-8971c760a991)

## 1 - Where to put the script?
The location of the folder can be found in GIMP itself. 
### For Linux owners and the users of the Microsoft’s Operating System:
Go to the top menu Edit → Preference, a Window opens, then on the left side, go to → Folders → Scripts, the location of your scripts folder is written on the right side of this panel.
### For the Apple’s macOS’ users:
Go to the top menu GIMP → Preferences, a Window opens, then on the left side, go to → Folders → Scripts, the location of your scripts folder is written on the right side of this panel.

![preferences](https://github.com/PatLabGit/GIMP-Export-All-Images-As/assets/47497693/c2beb57c-4c94-49c6-a0a0-ee5fcb68b9f9)

Once you did put the **_pxl-export-all-images-as.scm_** in the scripts folder, you can refresh all scripts directly from GIMP without closing GIMP.
To do so, just go to the top menu Filters → Script-Fu → Refresh Scripts. 
Or you can restart GIMP if you’re more comfortable with that.

## 2 - Where to find it in the menu?
In the top menu in GIMP, go to File → Export All Images As…, a sub-menu appears, chose the file type (As JPG, or as WebP, or as PNG, etc…) a window opens, here you go…

![screenshot_20231215-121856](https://github.com/PatLabGit/GIMP-Export-All-Images-As/assets/47497693/5b2e220e-80c7-46fc-b496-75b6f6ca8d89)

## 3 - How to use Export All Images As…? 
![screenshot_20231219-115008](https://github.com/PatLabGit/GIMP-Export-All-Images-As/assets/47497693/29f1ec5b-0c9e-4bde-8076-7b038b35e252)

1. Chose a folder where to export all your opened images.
2. _**Ask me to save the XCF when closing GIMP or an image**_: Is an option when later closing an image or GIMP itself, you will be asked if you want to save your image as XCF (all your layers and work will be saved in the GIMP’s XCF file format). Un-tick this option if you just want to export all your opened images without any further questions when closing GIMP or closing an already exported image.
3. Input a generic name that you want for your exported images, or you can let the default one.
4. Input a starting number, or you can left the default one, the script will auto increment the number by 1 for the next image. Your images will have a name like: IMAGE-00001.jpg, IMAGE-00002.jpg, etc…
5. This is the settings’ area of the image type you’ve selected previously (in the example above it’s JPG, size and settings will change for each image type, Except 1 – 2 - 3 – 4 and 6, as they will always show-up, for any type of images)
6. Select the way you want to interact while exporting your images (see below as it’s very important to understand this step).

## 4 - Fully automated or not?
There are 3 options that will define the way you inter-act or not during the exportation of all your opened images.
    (1) Automated: Above settings for all images, the one I do recommend, fully auto
    (2) Full Control: You confirm each setting on each image, if you want to inter-act for each image during exportation
    (3) Use Settings from latest Export Depending if your images were already exported or not, it will use the last setting or the setting you did input on the user interface, fully automated.

### The first option Automated: Above settings for all images
![screenshot_20231219-143439](https://github.com/PatLabGit/GIMP-Export-All-Images-As/assets/47497693/08d7c9b8-e257-4903-8701-1cd979474b51)

This option will use the setting that you input the first time, then will export ALL opened images with those settings. 
Depending the image type you want to export (WebP, PNG, JPG, etc…) you will have access to more or less settings, those settings are the settings that GIMP allows to use via a plugin or a script.
In all case, you input the accessible settings only once as it’s fully automated, means no more question asked, it will run without you inter-acting during the batch exportation.

### The second option Full Control: You confirm each setting on each image
![screenshot_20231225-153814](https://github.com/PatLabGit/GIMP-Export-All-Images-As/assets/47497693/231bab24-9221-4f21-90cd-f94c762a8c46)

Each time the script will export an image it will ask you to confirm the settings thru the user interface.
Those settings are default settings from GIMP or the ones you saved as default in GIMP, they will appear in the user interface for each image, thus you will have to just click OK to confirm or you can change the settings on the fly when the dialog appear for for exporting an image.
In all case it’s way faster than going to File → Export… each time, as the export dialog will automatically pop-ups each time the script will switch to the next opened image without any actions from you.

### The third option Use Settings from latest Export
![screenshot_20231225-153751](https://github.com/PatLabGit/GIMP-Export-All-Images-As/assets/47497693/e0010773-c118-4604-b03d-452ef51ed456)

This option can be automated, or manual depending the conditions in the image history.
1. Case one: You did not exported any image yet, selecting this option is like selecting the option Automated: Above settings for all images, this will take the settings you did input above, then all images are saved with these, it’s fully automated.
2. Case two: You have already batch exported your images at least once, but you continue to work on them, and want to export again with the very same settings you did your first export. **That’s the option to select** as this time the script will export each image with your previous settings, even if the previous settings were different from one image to another, and this process will be fully automated, no question asking, nor the need to confirm on a dialog. In this case the export will behave as Fully Automated, **but with a twist**, as it will use the latest settings you did use per image. Thus each image will be exported with their own latest export settings (or common settings if it was a batch export previously).
3. Case three: A part of opened images were already exported once, 
and a part of your opened images were never exported, but you want to re-export all at once as it’s easier, faster. Images which were already exported at least once will be fully auto with their latest export settings, Images which never were exported will use the settings you did input ABOVE that button for those newly/latest images, thus you will have a mix of automated latest settings export and settings that are actually in the user interface while you export.

## Tips and good to know:
**It replaces existing images**: Whatever mode you export your images, it won’t ask you if the image already exist  → what to do, it will just OVERWRITE it without any question!
Thus create a new folder to be sure to not replace the original images.

**You can do animated WebP** if you export, as you will be able to select “Animation” and other settings in the WebP dialog.
You can even use the option Use Settings From Latest Export, if you have to re-export your animation and want the same settings than the latest export.

**More about** **_Ask me to save the XCF when closing GIMP or an image_**: 
1. You’ve exported without this option checked: You will just have the file you have exported to (e.g. JPG, PNG, WebP, etc…), no reminder nor questions to save as XCF. Best if you just want the JPG, or PNG, or WebP, etc… Also useful with the Image → Slice Using Guides filter, and don’t want to keep the XCF of all new created images.
2. You’ve exported a second time but now you did activate this option: When closing GIMP or any image you did work on after this second “export”, you will be reminded to Save As... (XCF) those images you did work on, but not on the images you did not work on.
3. You’ve exported with this option checked (it is by default): When closing GIMP or an image, GIMP will ask you if you want to save the XCF, this will happen only when closing an image or GIMP itself
